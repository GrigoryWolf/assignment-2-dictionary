%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define BUFFER_SIZE 256
%define VALUE_TOO_LARGE_CODE 75
%define INVALID_ARG_CODE 22
%define POINTER_SIZE 8

section .rodata

error_message_not_found: db "Key was not found in dictionary", 0
error_message_overflow: db "The key more than buffer length", 0

section .bss

read_string_buffer: resb BUFFER_SIZE

section .text

global _start

_start:
    .read_word:
        mov rdi, read_string_buffer
        mov rsi, BUFFER_SIZE
        call read_word
        test rax, rax
        jz .overflow
    .find_word_in_dict:
        mov rdi, read_string_buffer
        mov rsi, BEGIN
        call find_word
        test rax, rax
        jz .not_found
    .found_word:
        mov rdi, rax
        call get_value_by_key
        mov rdi, rax
        call print_string
        xor rax, rax
        jmp exit
    .overflow:
        mov rdi, error_message_overflow
        call print_error_message
        mov rdi, VALUE_TOO_LARGE_CODE
        jmp exit
    .not_found:
        mov rdi, error_message_not_found
        call print_error_message
        mov rdi, INVALID_ARG_CODE
        jmp exit
