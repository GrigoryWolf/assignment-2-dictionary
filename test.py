import subprocess
input_file = "input.txt"
output_file = "output.txt"
codes_file = "codes.txt"

input_values = open(input_file, 'r')
outputs_values = open(output_file, 'r')
code_values= open(codes_file, 'r')

for codes, input_line, output_line in zip(code_values, input_values, outputs_values):
    process_result = subprocess.Popen(["./main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    std_out, std_err = process_result.communicate(input=input_line)
    out_line = std_out.strip()
    error_line = std_err.strip()
    code = int(codes)
    compared_output_value = output_line.strip()
    if out_line == compared_output_value and code==process_result.returncode and code==0:
        print("Tests pasts, input key is " + input_line)
        print("excepted value is " + compared_output_value)
        print("output value is " + out_line)
    else:
        print("Tests failed, input key is " + input_line)
        print("excepted value is " + compared_output_value)
        print("error is " + error_line.strip())
