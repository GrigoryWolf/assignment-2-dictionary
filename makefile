ASM = nasm
EFLAGS =- f elf64 -g
PYTHON = python3
.PHONY: all clean test
all: dict

%.o: %.asm
	$(ASM) $(EFLAGS) -o $@ $<
main:main.o lib.o dict.o
	ld -o $@ $^
clean:
	$(RM) dict *.o
test: 
	$(PYTHON) test.py