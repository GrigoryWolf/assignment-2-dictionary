%define next_entry 0

%macro colon 2
%ifid %2
	%2: dq next_entry
%else
	%error "Вторым аргументом должен быть ключом"
%endif
%ifstr %1
	db %1, 0
%else
	%error "Первым аргументом должна быть строка"
%endif

%define next_entry %2
%endmacro